import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  inputOptions: string; // variable for entering custom figure options for alternate
  // variables for entering custom shape options for standart
  figureTypes: string[];
  figureType: string;
  rectWidth: string;
  rectHeight: string;
  rectStartXCoordinate: string;
  rectStartYCoordinate: string;
  fill: string;
  fillOpacity: string;
  strokeFill: string;
  strokeWidth: string;
  pointCoordinates: string;
  circleRadius: string;
  circleCenterX: string;
  circleCenterY: string;
  ellipseRadiusX: string;
  ellipseRadiusY: string;
  lineX1: string;
  lineX2: string;
  lineY1: string;
  lineY2: string;
  dataInputForm: 'standart' | 'alternative'; // variable responsible for changing the input format

  ngOnInit(): void {
    this.figureTypes = [
      'rect', 'polygon', 'circle', 'line', 'polyline', 'ellipse'
    ];
    this.figureType = 'line';
    this.dataInputForm = 'standart';
  }

  // method that resets the values of input variables
  private resetInputValues(): void {
    this.rectWidth = '';
    this.figureType = 'line';
    this.rectHeight = '';
    this.rectStartXCoordinate = '';
    this.rectStartYCoordinate = '';
    this.strokeFill = '';
    this.fillOpacity = '';
    this.strokeWidth = '';
    this.fill = '';
    this.pointCoordinates = '';
    this.circleRadius = '';
    this.circleCenterX = '';
    this.circleCenterY = '';
    this.ellipseRadiusX = '';
    this.ellipseRadiusY = '';
    this.lineX1 = '';
    this.lineX2 = '';
    this.lineY1 = '';
    this.lineY2 = '';
    this.inputOptions = '';
  }

  // Method that sets the type of the figure
  setFigureType(figureType: string): void {
    this.figureType = figureType;
  }

  // method that dynamically adds an element to the dom svg according to the entered parameters
  createFigure(): void {
    const svgBox = document.querySelector('.box');
    switch (this.figureType) {

      case 'rect': {
        const rect = document.createElementNS('http://www.w3.org/2000/svg', 'rect');
        rect.setAttribute('width', `${this.rectWidth}`);
        rect.setAttribute('height', `${this.rectHeight}`);
        rect.setAttribute('x', `${this.rectStartXCoordinate}`);
        rect.setAttribute('y', `${this.rectStartYCoordinate}`);
        rect.setAttribute('fill', `${this.fill}`);
        rect.setAttribute('stroke', `${this.strokeFill}`);
        rect.setAttribute('stroke-width', `${this.strokeWidth}`);
        rect.setAttribute('fill-opacity', `${this.fillOpacity}`);
        svgBox.appendChild(rect);
        this.resetInputValues();
        break;
      }

      case 'polygon': {
        const polygon = document.createElementNS('http://www.w3.org/2000/svg', 'polygon');
        polygon.setAttribute('points', `${this.pointCoordinates}`);
        polygon.setAttribute('fill', `${this.fill}`);
        polygon.setAttribute('stroke', `${this.strokeFill}`);
        polygon.setAttribute('stroke-width', `${this.strokeWidth}`);
        polygon.setAttribute('fill-opacity', `${this.fillOpacity}`);
        svgBox.appendChild(polygon);
        this.resetInputValues();
        break;
      }

      case 'circle': {
        const circle = document.createElementNS('http://www.w3.org/2000/svg', 'circle');
        circle.setAttribute('r', `${this.circleRadius}`);
        circle.setAttribute('cx', `${this.circleCenterX}`);
        circle.setAttribute('cy', `${this.circleCenterY}`);
        circle.setAttribute('fill', `${this.fill}`);
        circle.setAttribute('stroke', `${this.strokeFill}`);
        circle.setAttribute('stroke-width', `${this.strokeWidth}`);
        circle.setAttribute('fill-opacity', `${this.fillOpacity}`);
        svgBox.appendChild(circle);
        this.resetInputValues();
        break;
      }

      case 'ellipse': {
        const ellipse = document.createElementNS('http://www.w3.org/2000/svg', 'ellipse');
        ellipse.setAttribute('rx', `${this.ellipseRadiusX}`);
        ellipse.setAttribute('ry', `${this.ellipseRadiusY}`);
        ellipse.setAttribute('cx', `${this.circleCenterX}`);
        ellipse.setAttribute('cy', `${this.circleCenterY}`);
        ellipse.setAttribute('fill', `${this.fill}`);
        ellipse.setAttribute('stroke', `${this.strokeFill}`);
        ellipse.setAttribute('stroke-width', `${this.strokeWidth}`);
        ellipse.setAttribute('fill-opacity', `${this.fillOpacity}`);
        svgBox.appendChild(ellipse);
        this.resetInputValues();
        break;
      }

      case 'line': {
        const line = document.createElementNS('http://www.w3.org/2000/svg', 'line');
        line.setAttribute('x1', `${this.lineX1}`);
        line.setAttribute('y1', `${this.lineY1}`);
        line.setAttribute('x2', `${this.lineX2}`);
        line.setAttribute('y2', `${this.lineY2}`);
        line.setAttribute('stroke', `${this.strokeFill}`);
        line.setAttribute('stroke-width', `${this.strokeWidth}`);
        svgBox.appendChild(line);
        this.resetInputValues();
        console.dir(svgBox);
        break;
      }

      case 'polyline': {
        const polyline = document.createElementNS('http://www.w3.org/2000/svg', 'polyline');
        polyline.setAttribute('points', `${this.pointCoordinates}`);
        polyline.setAttribute('fill', `${this.fill}`);
        polyline.setAttribute('stroke', `${this.strokeFill}`);
        polyline.setAttribute('stroke-width', `${this.strokeWidth}`);
        polyline.setAttribute('fill-opacity', `${this.fillOpacity}`);
        svgBox.appendChild(polyline);
        this.resetInputValues();
        break;
      }
    }
  }

  // Method that removes all svg elements created by the user
  public clearSvgBox(): void {
    const svgBox = document.querySelector('.box');
    while (svgBox.childNodes.length > 9) {
      svgBox.removeChild(svgBox.childNodes[9]);
    }
    console.dir(svgBox);
  }

  // a methods that sets the parameters of a shape from the entered data in an alternative form
  setInputsData(): void {
    const figureType = this.getFigureTypeFromInputOptions();
    console.log(figureType);
    if (!this.figureTypes.includes(figureType)) {
      alert('please input correct figure type!');
      return;
    } else {
      this.figureType = figureType;
    }

    switch (figureType) {
      case 'polyline': {
        this.setFigurePropertiesByInputOptions(['-p', '-f', '-sw', '-sf', '-fo']);
        break;
      }
      case 'rect': {
        this.setFigurePropertiesByInputOptions(['-w', '-h', '-rx', '-ry', '-f', '-sw', '-sf', '-fo']);
        break;
      }
      case 'line': {
        this.setFigurePropertiesByInputOptions(['-x1', '-x2', '-y1', '-y2', '-sw', '-sf']);
        break;
      }
      case 'circle': {
        this.setFigurePropertiesByInputOptions(['-cr', '-cx', '-cy', '-f', '-sw', '-sf', '-fo']);
        break;
      }
      case 'polygon': {
        this.setFigurePropertiesByInputOptions(['-p', '-f', '-sw', '-sf', '-fo']);
        break;
      }
      case 'ellipse': {
        this.setFigurePropertiesByInputOptions(['-erx', '-ery', '-cx', '-cy', '-f', '-sw', '-sf', '-fo']);
        break;
      }
    }
    console.log(this.figureType, this.strokeFill, this.pointCoordinates, this.strokeWidth);
    this.createFigure();
  }

  private getFigureTypeFromInputOptions(): string {
    const figureTypeEndChar = this.inputOptions.indexOf(' ');
    const figureType = this.inputOptions.slice(0, figureTypeEndChar);
    console.log('figureType', figureType);
    if (!this.figureTypes.includes(figureType)) {
      alert('please put correct figure type!');
    } else {
      return figureType;
    }
  }

  private setFigurePropertiesByInputOptions(abbreviations: string[]): void {
    abbreviations.forEach(abbreviation => {
      const startCharSetProperty = this.inputOptions.indexOf(abbreviation);
      const startCharSetPropertyValue = this.inputOptions.indexOf('[', startCharSetProperty) + 1;
      const endCharSetPropertyValue = this.inputOptions.indexOf(']', startCharSetProperty);
      console.log('startCharSetProperty', startCharSetProperty, 'endCharSetProperty', endCharSetPropertyValue);
      const setPropertyNewValue = this.inputOptions.slice(startCharSetPropertyValue, endCharSetPropertyValue);
      switch (abbreviation) {
        case '-f': {
          this.fill = setPropertyNewValue;
          break;
        }
        case '-fo': {
          this.fillOpacity = setPropertyNewValue;
          break;
        }
        case '-sw': {
          this.strokeWidth = setPropertyNewValue;
          break;
        }
        case '-w': {
          this.rectWidth = setPropertyNewValue;
          break;
        }
        case '-h': {
          this.rectHeight = setPropertyNewValue;
          break;
        }
        case '-rx': {
          this.rectStartXCoordinate = setPropertyNewValue;
          console.log(setPropertyNewValue);
          break;
        }
        case '-ry': {
          this.rectStartYCoordinate = setPropertyNewValue;
          console.log(setPropertyNewValue);
          break;
        }
        case '-p': {
          this.pointCoordinates = setPropertyNewValue;
          break;
        }
        case '-cx': {
          this.circleCenterX = setPropertyNewValue;
          break;
        }
        case '-cy': {
          this.circleCenterY = setPropertyNewValue;
          break;
        }
        case '-erx': {
          this.ellipseRadiusX = setPropertyNewValue;
          break;
        }
        case '-ery': {
          this.ellipseRadiusY = setPropertyNewValue;
          break;
        }
        case '-cr': {
          this.circleRadius = setPropertyNewValue;
          break;
        }
        case '-x1': {
          this.lineX1 = setPropertyNewValue;
          break;
        }
        case '-x2': {
          this.lineX2 = setPropertyNewValue;
          break;
        }
        case '-y1': {
          this.lineY1 = setPropertyNewValue;
          break;
        }
        case '-y2': {
          this.lineY2 = setPropertyNewValue;
          break;
        }
        case '-sf': {
          this.strokeFill = setPropertyNewValue;
          break;
        }
      }
      console.log('setPropertyNewValue', setPropertyNewValue, this.strokeWidth);
    });
  }

  // method that changes the input form
  setInputForm(): void {
    if (this.dataInputForm === 'alternative') {
      this.dataInputForm = 'standart';
    } else {
      this.dataInputForm = 'alternative';
    }
  }
}
